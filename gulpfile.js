'use strict'

const browserSync = require('browser-sync');
const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');

gulp.task('sass', function(done){
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
        console.log("CSS changes done");
        done();
});

gulp.task('sass:watch', function(){
    gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function(){
    var files = ['./*html', './css/*.css', './img/*.{png, jpg, gid}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir:'./'
        }
    });
});

gulp.task('default', gulp.series('browser-sync', function(){
    gulp.start('sass:watch');
}));

gulp.task('clean', function(){
    return del(['dist']);
});

gulp.task('copyfonts', function(done){
    gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
    .pipe(gulp.dest('./dist/fonts'));
    console.log("copyfonts changes done");
    done();
});

gulp.task('imagemin', function(){
    return gulp.src('./images/*')
    .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('usemin', function(){
    return gulp.src('./*html')
        .pipe(flatmap(function(stream, file){
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function() {return htmlmin({collapseWhitespace: true})}],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                }))
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('build',gulp.series('clean', 'copyfonts','imagemin','usemin'));

// gulp.task('build', gulp.series('clean', function(){
//     gulp.start('copyfonts', 'imagemin', 'usemin');
// }));